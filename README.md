### Web Speech iOS mishap
#### Simple attempt to run [Web Speech API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API) (TTS) from Unity WebGL build

The main script is **WebSpeechCaller.cs** on *Scripter* GameObject in **CallingTTS** scene. 

First followed [Unity manual](https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html). Implemented ```TriggerInternalSpeech``` method calls a simple speech sequence imported via **WebSpeechPlugin.jslib**. This works fine on [all platforms & browsers compatible](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API#SpeechSynthesis) except for iOS. (Methods seem to complete on all platforms, there's just no sound on iOS.)

Hence used [obsolete Application.ExternalCall](https://docs.unity3d.com/ScriptReference/Application.ExternalCall.html) in ```TriggerExternalSpeech``` method, which calls ```readText``` from WebGL template **index.html** in *Assets/WebGLTemplates/SideLoad* directory. This also works everywhere but on iOS. 

Out of curiosity, added a button to the same html file and to trigger JS TTS function and it DOES work, can hear the sound even on iOS. So added ```indirectReading``` method, which was meant to programatically 'click' button from within html file. Yet again, works everywhere but on iOS.

In above-described cases methods complete running on all platforms, there's just no sound on iOS if the call originates from WebGL.

**CallingTTS** scene has a console, which makes it easier to test on mobiles.

The project built in Unity 2019.3.15f1. Switch the build target to WebGL and make sure you use 'SideLoad' WebGL Template in Player settings.