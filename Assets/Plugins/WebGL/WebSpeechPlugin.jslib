mergeInto(LibraryManager.library, {

    Hello: function () {
        window.alert("Hello, world!");
    },

    GetItSynthed: function (textRead) {
        var synth = window.speechSynthesis;
        var voices = synth.getVoices();
        var speakText = new SpeechSynthesisUtterance(Pointer_stringify(textRead));
        for (var voiceIndex = 0; voiceIndex < voices.length; ++voiceIndex) {
            var voice = voices[voiceIndex];
            var voiceLang = voices[voiceIndex].lang
            if (new String(voiceLang).valueOf() == new String('en-GB').valueOf()) {
                speakText.voice = voice;
                break;
            }
        }
        synth.speak(speakText);
        unityInstance.SendMessage('Scripter', 'ProcessingFeedback', 'Internal call spoke')
    },
});