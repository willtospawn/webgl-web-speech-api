﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class WebSpeechCaller : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void GetItSynthed(string str);
    
    public delegate void LogHandler(string text);
    public event LogHandler OnMessageLogged;


    public Button externalButton, internalButton;
    private const string ToRead = "read this please until you can be heard";

    private bool _direct = true;
    private void Awake()
    {
        externalButton.onClick.AddListener(TriggerExternalSpeech);
        internalButton.onClick.AddListener(TriggerInternalSpeech);
    }

    /// <summary>
    /// WebGLTemplates/SideLoad/index.html
    /// </summary>
    private void TriggerExternalSpeech()
    {
        // Log("Pressed Trigger External");
        if (_direct)
            Application.ExternalCall("readText", ToRead);
        else 
            Application.ExternalCall("indirectReading");
        _direct = !_direct;
    }

    // Plugin/WebGL/WebSpeechPlugin.jslib
    private void TriggerInternalSpeech()
    {
        // Log("Pressed Trigger Speech");
        GetItSynthed(ToRead);
    }

    /// <summary>
    /// Called from JS to log into the console
    /// </summary>
    /// <param name="source"></param>
    public void ProcessingFeedback(string source)
    {
        Log($"The latest event: {source}"); 
    }
    
    private void Log(string text) {
        var message = $"Executed - {text}";

        Debug.Log(message);

        if (OnMessageLogged != null) {
            OnMessageLogged(message);
        }
    }
}
