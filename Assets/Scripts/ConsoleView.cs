﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleView : MonoBehaviour {

    public Button clearButton;

    public ScrollRect scrollRect;

    public Text consoleText;

    private void Awake() {
        consoleText.text = string.Empty;

        clearButton.onClick.AddListener(Clear);
    }

    private void Start() {
        Application.logMessageReceived += (condition, stackTrace, type) => {
            Append(condition);
        };
    }

    private void Append(string text) {        
        consoleText.text += text + Environment.NewLine;

        scrollRect.verticalNormalizedPosition = 0;
    }

    private void Clear() {
        consoleText.text = string.Empty;

        scrollRect.verticalNormalizedPosition = 1;
    }
}